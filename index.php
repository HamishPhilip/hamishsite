<?php namespace presentation;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$file=$_SERVER['DOCUMENT_ROOT']."/include/appControl/appControlBl.php";
include_once($file);
$file=$_SERVER['DOCUMENT_ROOT']."/include/pl.php";
include_once($file);
use prestationLogic\Pl as PL;
use bizLogic\AppControlBL as BL;
class Present extends BL
{
    private $myName="index.php";
    private $alertMsg="";
    public function __construct()
    {
        $specialLogAction="NoAction";
        PL::getEnv($this->myName);
        $this->alertMsg=PL::handleAlerts($this->myName);
        parent::__construct();
        $specifcReplacesArray=parent::buildPage($this->myName);
        // $LogedInOutVars=explode("|",$specifcReplacesArray['loggedInOutTxt']);
        // $loggedInOutTxt=$LogedInOutVars[0];
        // if(isset($this->currentUserArray)){
        //     echo("<br>Array:params(".__LINE__."(".__METHOD__."))<br><pre>"); print_r($this->currentUserArray); echo("</pre><hr>");
        //     $loggedInOutTxt=$LogedInOutVars[1];
        // }
        /*ToDo:: when profiles are active add the avatar*/
        $packDrill="<!-not logged in-->";
        if(isset($_SESSION['us'][1])){
            // ToDo;  Checjk after login
            //contentActions
            $packDrill="Do Something Here"; // get from db
        }
        $alert="";
        if(strlen($this->alertMsg)>5){
            $alert.=$specifcReplacesArray['alertScript'];
            $alert=str_replace("[[alertMsg]]",$this->alertMsg,$alert);  
        }
        //ToDo:: fix once login is active
        $loggedInOutTxt="Not Logged in";
        $this->ht=str_replace("[[packDrill]]",$packDrill,$this->ht);
        $this->ht=str_replace("//scrAlert",$alert,$this->ht);
        $this->ht=str_replace("[[loggedInOutTxt]]",$loggedInOutTxt,$this->ht);
        $this->ht=str_replace("[[menuAdded]]",$this->addedMenu,$this->ht);
        $this->ht=str_replace("[[currentPageUrl]]",$this->myName.".php",$this->ht);
        parent::writeLogs("{$this->myName}::$specialLogAction");   
        $Objvars=PL::registerPLVars(get_object_vars($this),2);
        parent::traceObj(__METHOD__,"$Objvars","Construct:{$this->myName}");
        echo($this->ht);
    }
}
new Present;


