<?php namespace presentation;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$file=$_SERVER['DOCUMENT_ROOT']."/include/appControl/appControlBl.php";
include_once($file);
$file=$_SERVER['DOCUMENT_ROOT']."/include/pl.php";
include_once($file);
use prestationLogic\Pl as PL;
use bizLogic\AppControlBL as BL;
class Present extends BL
{
    private $myName="__handler__.php";
    public function __construct()
    {
        $specialLogAction="NoAction";
        PL::getEnv($this->myName);
        $this->alertMsg=PL::handleAlerts($this->myName);
        parent::__construct();
        if(isset($_POST)){
            //parent::debugPrint($this->myName."::line::".__LINE__,$_POST);
            $pstArray=$_POST;
            unset($_POST);
            $response=PL::evalPosts($pstArray);
            parent::writeLogs("{$this->myName}::$specialLogAction");   
            $Objvars=PL::registerPLVars(get_object_vars($this),1);
            parent::traceObj(__METHOD__,"$Objvars","Construct:{$this->myName}");            
            parent::evalForms($response);   // this will rediredt do we need to be done with this obj
        }

    }
}
new Present;


