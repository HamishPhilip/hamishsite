<?php namespace bizLogic;

if (session_status() == PHP_SESSION_NONE) {
    file_put_contents(".errlog","\n".date("Ymd H:i:s")."Violation::No session for".__FILE__,FILE_APPEND);
    exit();
}
$fl="{$_SERVER['DOCUMENT_ROOT']}/include/dl/dlService.php";
include_once($fl);
$ml="{$_SERVER['DOCUMENT_ROOT']}//include/mail/mailService.php";
include_once($ml);
use dataLogic\DlService as DL;
use mailService\Send as sendMailNow;
class AppControlBL extends DL
{
    /*control vars */
    private $myName="AppControlBL";
    public $currentUserArray;
    public $currentTokenArray;
    public $adminMail="wayne.h.philip@gmail.com";
    public $ht_overall;
    public $siteId;
    //public page_vars
    public $pageArray;
    //public $applications;
    public $userApps;
    public $ht;
    public $appMessagesLocation="php.forms/app_cttl/read_messages.p.php";
    public $mail_address;
    public $mail_subject;
    public $mail_body;
    public $addedMenu;
    public $mail_response;
    public $logInOutMenuTxt="<li><a href=\"login.php\" class=\"button fit\">Login</a></li>";
    //-----
    public $validateUser=false;
    public $scriptPointers=array();
    public $traceActivity=false;
    public $urlArray;
    public $menuAddedArray;
    public $applicationArray;
    public $urlProfilesArray;
    public $profileUserArray;
    public $permissionsArray;
    public $plAllertArray=array();
    public $crudAutoArray=array("Math4You Login Token|10",
        "Maths4youNotice|11",
        "Maths4youError|12",
        "Contact with Maths4You.Today|13",
    );
    //------Auth
    private $authStsArray=array(
        "cfcd208495d565ef66e7dff9f98764da"=>"0|Unauthenticated",
        "45c48cce2e2d7fbdea1afc51c7c6ad26"=>"9|TokenSet",
        "c4ca4238a0b923820dcc509a6f75849b"=>"1|LoggedIn"
    );
    private $authSessUs="0->email|1->";
    //public $urlPackDrillArray;
    //-------
    public function __construct()
    {
        parent::__construct();
        $this->handleSession();
        $this->siteId=getenv("websiteId");
    }
    public function evalForms(array $posts)                       //2101      25
    {   //waynep1
        //buildPage__METHOD__,implode("!",$posts),"__handler__.php");
        switch ($posts['pst_from']) {
            case "logIn.php":
                $this->currentUserArray=$this->getUserByEmail($posts['pst_email']);      
                $this->debugPrint("BL::line::".__LINE__,$this->currentUserArray);        
                if(is_null($this->currentUserArray)){
                    $_SESSION['alert'][$posts['pst_from']]="Sorry we do not know that address ({$posts['pst_email']}). You may need to register:b:".__LINE__;
                    header("Location: {$posts['pst_from']}",301);
                    exit();
                }
                $mailReturn=$this->sendUserLoginToken($this->currentUserArray['email']);
                if($mailReturn['sendStatus']==1){ //success
                    $usEncr=$this->enCryp($this->currentUserArray['email']);
                    $_SESSION['us'][0]=$usEncr;
                    $_SESSION['alert']['logInValidate.php']="Login token sent.";
                    header("Location: logInValidate.php",301);
                    exit();break;
                }
                $_SESSION['alert']['index.php']="Login token could not be sent. Please retry:b:".__LINE__;
                header("Location: index.php",301);
                exit();break;      
            case "logInValidate.php":
                if(!isset($_SESSION['us'][0])){ //testMe
                    $_SESSION['alert']['index.php']="Sorry..Login failure. Please retry:b:".__LINE__;
                    header("Location: index.php",301);
                    exit();break;
                }
                $this->currentUserArray=$this->getUserByUsSess();
                $setTokenArray=$this->getToken($this->currentUserArray['id'],1);
                $this->debugPrint("BL::line::".__LINE__,$posts);
                $this->debugPrint("BL::line::".__LINE__,$setTokenArray);
                if($posts['pst_token']==$setTokenArray['token']){
                    $_SESSION['us'][1]=$this->enCryp($this->currentUserArray['status']);
                    $_SESSION['us'][2]=$this->enCryp($this->currentUserArray['status']);
                    if($this->currentUserArray['status']==1){
                        
                        //ToDo:: Get To Subscriptions change [1] above
                    }
                }


                //waynep1
                $_SESSION['alert']['index.php']="dbb:b:".__LINE__;
                    header("Location: index.php",301);
                    exit();break;
            default:
                $_SESSION['alert']['index.php']="UnKnown Form {$posts['pst_from']}:b:".__LINE__;
                header("Location: index.php",301);
                exit();  
                break;
        }
    }
    public function buildPage(string $pageName)                 // 2101 -23
    {       
        //buildPage__METHOD__,$pageName,debug_backtrace()[1]['function']);    
        $this->getPage($pageName);   
        // echo("<br>Array:params(".__LINE__."(".__METHOD__."))<br><pre>"); print_r($this->pageArray); echo("</pre><hr>"); 
        $this->ht=file_get_contents("templates/{$this->pageArray['page']['hddr_template']}");
        $this->ht.=file_get_contents("templates/{$this->pageArray['page']['body_template']}");
        $this->ht.=file_get_contents("templates/{$this->pageArray['page']['footer_template']}");
        $specifcReplacesArray=$this->pageFixPage();
        $this->unSetSessionVars();
        return $specifcReplacesArray;
    }
    private function pageFixPage()
    {
        /* ToDo::delete as you go */
        //buildPage__METHOD__,"intrisic(this->pageArray['page'])",debug_backtrace()[1]['function']);
        $specifcReplacesArray=array();
        //page
        $stdArray=array('title','logo_txt','info_image');
        // for ($i = 0; $i < count($stdArray); $i++) {
        //     $replace="[[{$stdArray[$i]}]]";
        //     $value=$this->pageArray['page'][$stdArray[$i]];
        //     $this->ht=str_replace($replace,$value,$this->ht);
        // }
        //style_include
        $value=file_get_contents("css/{$this->pageArray['page']['style']}");
        $this->ht=str_replace("[[styleSheet]]",$value,$this->ht);
        // //page_fixed
        // for ($i = 0; $i < count($this->pageArray['page_fixed']); $i++) {
        //     $replace="[[{$this->pageArray['page_fixed'][$i]['position_name']}]]";
        //     $value=$this->pageArray['page_fixed'][$i]['ht'];
        //     $this->ht=str_replace($replace,$value,$this->ht);
        // }       
        // // page_images
        // for ($i = 0; $i < count($this->pageArray['page_images']); $i++) {
        //     if($this->pageArray['page_images'][$i]['conditional']==0){
        //         $replace="[[{$this->pageArray['page_images'][$i]['position_name']}]]";
        //         $value=$this->pageArray['page_images'][$i]['image'];
        //         $this->ht=str_replace($replace,$value,$this->ht);                
        //     }
        //     else{
        //         $specifcReplacesArray["{$this->pageArray['page_images'][$i]['position_name']}"]=$this->pageArray['page_images'][$i]['image'];
        //     }            
        // }       
        // page_scripts
        // for ($i = 0; $i < count($this->pageArray['page_scripts']); $i++) {
        //     if($this->pageArray['page_scripts'][$i]['conditional']==0){
        //         $replace="[[{$this->pageArray['page_scripts'][$i]['position_name']}]]";
        //         $value=$this->pageArray['page_scripts'][$i]['script'];
        //         $this->ht=str_replace($replace,$value,$this->ht);
        //     }
        //     else{
        //         $specifcReplacesArray["{$this->pageArray['page_scripts'][$i]['position_name']}"]=$this->pageArray['page_scripts'][$i]['script'];
        //     }
        // }     
        // page_texts
        for ($i = 0; $i < count($this->pageArray['page_texts']); $i++) {
            if($this->pageArray['page_texts'][$i]['conditional']==0){
                $replace="[[{$this->pageArray['page_texts'][$i]['position_name']}]]";
                $value=$this->pageArray['page_texts'][$i]['text'];
                $this->ht=str_replace($replace,$value,$this->ht);
            }
            else{
                $specifcReplacesArray["{$this->pageArray['page_texts'][$i]['position_name']}"]=$this->pageArray['page_texts'][$i]['text'];
            }
        }                  
        return $specifcReplacesArray;
    }
    private function getPage(string $pageName)                          //2101  24
    {   
        //buildPage__METHOD__,"page_name=$pageName",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbWebsite;
        $this->tbl='pages';
        $this->wc="(page_name = '$pageName') AND (status=1) AND (site_id={$this->siteId})";
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        $this->pageArray['page']=$this->flattenArray($this->recordSetArray);
        $id=$this->pageArray['page']['id'];      
        // $this->pageArray['page_images']=$this->recordSetArray;   
        //texts
        $this->dbBase=$this->dbWebsite;
        $this->tbl='page_texts';
        $this->wc="(page_id = $id) AND (status=1)";
        parent::connectDatabase();
        parent::sqlSetRecordset();     
        $this->pageArray['page_texts']=$this->recordSetArray;   
        return;
    }
    public function validateQuizUsage(int $isageType)
    {
        //buildPage__METHOD__,"Array objVars");
        if(!isset($_SESSION['us'])){
            return -9; //auth required
        }
        $emailCde=$this->deEncrypt($_SESSION['us'][1]);
        $this->currentUserArray=$this->getUserByEmailCode($emailCde);
        if(is_null($this->currentUserArray)){
            return 99; //user not found
        }
        return $this->currentUserArray['status'];

        if($this->currentUserArray['sts']==20){
            // ToDo
        }
    }
    public function validateEmailCode(string $code)
    {
        //2101  -07              
        //buildPage__METHOD__,"emailCode=$code",debug_backtrace()[1]['function']);
        $emailMD=$this->deEncrypt($code);
        $userArray=$this->getUserByEmailCode($emailMD);
        if(!is_null($userArray)){
            if($userArray['status']==-100){
                $userArray['id'];
                $this->setToken($userArray['id'],1,2);
                $this->setUserStatus($userArray['id'],1);
                $returnArray=$this->getUserById($userArray['id']);
                return $returnArray;
            }
        }
        return array();
    }
    public function setTokenStatus(int $id, int $sts)       //  2101    23
    {
        //waynep
        //buildPage__METHOD__,"id=$id|sts=$sts",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbAppControl;
        $this->tbl="tokens";        
        $this->wc="id = $id";
        $this->updates="status = $sts";  
        parent::connectDatabase();
        parent::sqlUpdate();   
    }
    public function unsetExpiredTokens()                    //  2101    23
    {
        //waynep
        //buildPage__METHOD__,"Null",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbAppControl;
        $this->tbl="tokens";        
        $this->wc="(NOW() > expires) AND (status > -99)";
        $this->orderBy="id DESC";
        $this->limit="LIMIT 5";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(!is_null($this->recordSetArray)){
            $rs=$this->recordSetArray;
            for ($i = 0; $i < count($rs); $i++) {
                $this->setTokenStatus($rs[$i]['id'],-99);
            }
        }           
    }
    //gets here
    public function getComsStatusAll()
    {
        //2101  -07 
        //buildPage__METHOD__,"Null",debug_backtrace()[1]['function']);
        $this->dbBase=getenv("dbCrud");
        $this->tbl="coms_in_store_status";        
        $this->orderBy="id ASC";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        return $this->recordSetArray;
    }
    public function getComsByStatus(int $comsFilter=0)
    {
        //2101  -07 
        //buildPage__METHOD__,"commsFilter(status)=$comsFilter",debug_backtrace()[1]['function']);
        $this->setAllAutoComs();
        $this->dbBase=getenv("dbCrud");
        $this->tbl="coms_in_store";        
        $this->wc="status=$comsFilter";
        $this->orderBy="id ASC";
        $this->limit="LIMIT 10";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        $rs=$this->recordSetArray;
        // channel Text add
        for($n=0; $n<count($rs); $n++){
            $foundTxt="UnKnownChannel";
            $this->tbl="coms_channels";        
            $this->wc="id={$rs[$n]['channel_id']}";
            $this->limit="LIMIT 1";
            parent::connectDatabase();
            parent::sqlSetRecordset();      
            if(isset($this->recordSetArray[0]['id'])){
                $foundTxt=$this->recordSetArray[0]['display_txt'];
            }     
            $rs[$n]['substChannel']=$foundTxt;
        }        
        // ststus text add
        for($n=0; $n<count($rs); $n++){
            $foundTxt="UnKnownStatus";
            $this->tbl="coms_in_store_status";        
            $this->wc="id={$rs[$n]['status']}";
            $this->limit="LIMIT 1";
            parent::connectDatabase();
            parent::sqlSetRecordset();      
            if(isset($this->recordSetArray[0]['id'])){
                $foundTxt=$this->recordSetArray[0]['status_txt'];
            }     
            $rs[$n]['substStatus']=$foundTxt;
        } 
        //find members    
        for($n=0; $n<count($rs); $n++){
            $userFrom=$this->getUserByEmail($rs[$n]['from_address']);
            if(isset($userFrom['id'])){
                $rs[$n]['from_address'].="<span class=\"orangeColor\">&nbsp;&nbsp;Member({$userFrom['status']})&nbsp;&nbsp;</span>";
            }
            $userTo=$this->getUserByEmail($rs[$n]['to_address']);
            if(isset($userTo['id'])){
                $rs[$n]['to_address'].="<span class=\"orangeColor\">&nbsp;&nbsp;Member({$userTo['status']})&nbsp;&nbsp;</span>";
            }            
        }        
        return $rs;
    }
    public function getTokensByUserId(int $id, string $typeString=">1")
    {
        $this->unsetExpiredTokens();
        $this->dbBase=$this->dbAppControl;
        $this->tbl="tokens";        
        $this->wc="(user_id=$id) AND (token_type_id $typeString)";
        $this->orderBy="expires DESC";
        $this->limit="LIMIT 5";
        parent::connectDatabase();
        parent::sqlSetRecordset();
    }
    ////sets here
    private function setAllAutoComs(){
        $this->dbBase=getenv("dbCrud");      
        for($n=0;$n<count($this->crudAutoArray);$n++){
            $this->tbl="coms_in_store";   
            $item=explode("|",$this->crudAutoArray[$n]);
            $findSubject=$item[0];
            $sts=$item[1];
            $this->wc="(status=0) AND (subject = '$findSubject')";
            $this->updates="status=$sts";
            parent::connectDatabase();
            parent::sqlUpdate();
        }
    }
    public function setComsQueueStatus(int $id, int $sts)
    {
        //2101  -07 
        //buildPage__METHOD__,"id=$id::status=$sts",debug_backtrace()[1]['function']);
        $this->dbBase=getenv("dbCrud");      
        $this->tbl="coms_in_store";   
        $this->wc="id=$id";
        $this->updates="status=$sts";
        parent::connectDatabase();
        parent::sqlUpdate();
    }
    private function setUserStatus(int $id, int $setSts)
    {
        $this->dbBase=$this->dbAppControl;
        $this->tbl="users";
        $this->wc="id=$id";
        $this->updates="status=$setSts";
        parent::connectDatabase();
        parent::sqlUpdate();
        return;
    }

    public function getPatreonUser($email)
    {
        //buildPage__METHOD__,$email);
        $this->dbBase=$this->dbAppControl;
        $this->tbl='users';
        $this->wc="email='$email'";
        $this->limit="LIMIT 1";     
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }
    public function writeUserCheckPatreon($pUser)
    {
        //buildPage__METHOD__,"Array=pUser");
        $this->dbBase=$this->dbAppControl;
        // lets try and get the user first
        $email=$pUser['data']['attributes']['email'];
        $found=$this->getPatreonUser($email);
        if(is_null($found))
        {
            // create User here
            $this->dbBase=$this->dbAppControl;
            $this->tbl='users';
            $mcde=md5($pUser['data']['attributes']['email']);
            $this->inserts="
                 userid='{$pUser['data']['id']}',"
                ."password='Patreon',"
                ."email='{$pUser['data']['attributes']['email']}',"
                ."mcde='$mcde',"
                ."first_name='{$pUser['data']['attributes']['first_name']}',"
                ."full_name='{$pUser['data']['attributes']['full_name']}',"
                ."is_email_verified='{$pUser['data']['attributes']['is_email_verified']}',"
                ."last_name='{$pUser['data']['attributes']['last_name']}',"
                ."thumb_url='{$pUser['data']['attributes']['thumb_url']}',"
                ."patreon_url='{$pUser['data']['attributes']['url']}',"
                ."patreon_vanity='{$pUser['data']['attributes']['vanity']}',"
                ."patreon_id='{$pUser['data']['id']}',"
                // ."patreon_relationships_id='{$pUser['data']['attributes']['memberships']['data'][0]['id']}',"
                // ."patreon_relationships_type='{$pUser['data']['attributes']['memberships']['data'][0]['type']}',"
                ."patreon_currently_entitled_amount_cents={$pUser['included'][0]['attributes']['currently_entitled_amount_cents']},";
            $patreon_last_charge_date=substr($pUser['included'][0]['attributes']['last_charge_date'],0,10);
            $pledge_relationship_start=substr($pUser['included'][0]['attributes']['pledge_relationship_start'],0,10);
            $this->inserts.="patreon_last_charge_date='$patreon_last_charge_date}',"
                ."patreon_pledge_relationship_start='$patreon_last_charge_date}',"
                ."patreon_last_charge_status='$pledge_relationship_start',"
                ."patreon_lifetime_support_cents='{$pUser['included'][0]['attributes']['lifetime_support_cents']}',"
                ."patreon_patron_status='{$pUser['included'][0]['attributes']['patron_status']}',"
                ."patroopn_link_self='{$pUser['links']['self']}',";
            $sts=0;
            if($pUser['included'][0]['attributes']['patron_status']=='active_patron'){
                $sts=1;
            }
            $SysCheckTime=time();
            $this->inserts.="status=$sts,sys_check=$SysCheckTime";
            parent::sqlInsert();
            $insertedId=$this->sqlLastId;
            if($insertedId>0){
                return 1;
            }
            return 0;
        }
        //update records
        $this->dbBase=$this->dbAppControl;
        $this->tbl='users';
        $this->updates=""
            ."first_name='{$pUser['data']['attributes']['first_name']}',"
            ."full_name='{$pUser['data']['attributes']['full_name']}',"
            ."is_email_verified='{$pUser['data']['attributes']['is_email_verified']}',"
            ."last_name='{$pUser['data']['attributes']['last_name']}',"
            ."thumb_url='{$pUser['data']['attributes']['thumb_url']}'," 
            ."thumb_url='{$pUser['data']['attributes']['thumb_url']}',"
            ."patreon_url='{$pUser['data']['attributes']['url']}',"
            ."patreon_vanity='{$pUser['data']['attributes']['vanity']}',"
            ."patreon_id='{$pUser['data']['id']}',"
            // ."patreon_relationships_id='{$pUser['data']['attributes']['memberships']['data'][0]['id']}',"
            // ."patreon_relationships_type='{$pUser['data']['attributes']['memberships']['data'][0]['type']}',"
            ."patreon_currently_entitled_amount_cents={$pUser['included'][0]['attributes']['currently_entitled_amount_cents']},";
        $patreon_last_charge_date=substr($pUser['included'][0]['attributes']['last_charge_date'],0,10);
        $pledge_relationship_start=substr($pUser['included'][0]['attributes']['pledge_relationship_start'],0,10);
        $this->updates.="patreon_last_charge_date='$patreon_last_charge_date}',"
            ."patreon_pledge_relationship_start='$patreon_last_charge_date}',"
            ."patreon_last_charge_status='$pledge_relationship_start',"
            ."patreon_lifetime_support_cents='{$pUser['included'][0]['attributes']['lifetime_support_cents']}',"
            ."patreon_patron_status='{$pUser['included'][0]['attributes']['patron_status']}',"
            ."patroopn_link_self='{$pUser['links']['self']}',";
        $sts=0;
        if($pUser['included'][0]['attributes']['patron_status']=='active_patron'){
            $sts=1;
        }
        $SysCheckTime=time();
        $this->updates.="status=$sts,sys_check=$SysCheckTime";
        $this->wc="email='$email'";
        parent::sqlUpdate();
        $updCheckId=$this->sqlLastId;
        if($updCheckId>0){
            return 1;
        }
        return 0;
    }
    public function writeLogs(string $newContent="UnKnown::FixMe::".__LINE__)
    {   //2101  -07   
        //buildPage__METHOD__,"$newContent",debug_backtrace()[1]['function']);
        $ddtt=time();
        $logsContent="$ddtt=>$newContent\n";
        $eml="";
        if(isset($_SESSION['us'])){
            $eml=implode("|",$_SESSION['us']);
        }
        $ip=$_SERVER['REMOTE_ADDR'];
        $sid=session_id();
        $this->dbBase=$this->dbAppControl;
        $this->tbl="logs";
        $sid=session_id();
        $this->wc="session_id='$sid'";
        $this->limit="Limit 1";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            // update record
            $logsContent=$logsContent.$this->recordSetArray[0]['access_trace'];
            $this->tbl="logs";  
            $this->wc="session_id='$sid'";
            $this->updates="access_trace='$logsContent'";
            parent::sqlUpdate();
            return;
        }
        $this->dbBase=$this->dbAppControl;
        $this->tbl="logs";       
        $this->inserts="session_id='$sid', access_trace='$logsContent', ip='$ip', email='$eml'";
        parent::connectDatabase();
        parent::sqlInsert();
        return;
    }
    public function getMultiChoiseQuestion( int $qId)
    {
        //buildPage__METHOD__,"$qId");
        $this->tbl='multi_choice_questions';
        $this->wc="id = $qId";
        $this->dbBase=$this->dbMaths;
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;    
    }
    public function getSpecificMultiChoiseQuestion(int $questionId)
    {
        //buildPage__METHOD__,$questionId);
        $this->tbl='multi_choice_questions';
        $this->wc="id  = $questionId";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;    
    }    
    public function getMultiChoiseOptions( int $questionID)
    {
        //buildPage__METHOD__,$questionID);
        $this->tbl='multi_choice_options';
        $this->wc="(question_id=$questionID) and (status = 1)";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;    
    }    
    public function getAllMultiChoiseQuestions( int $lesson_id)
    {
        //buildPage__METHOD__,$lesson_id);
        $this->tbl='multi_choice_questions';
        $this->wc="(lesson_id = $lesson_id )and (status = 1)";
        $this->orderBy="id ASC";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;    
    }    
    public function getLessonBySubSectionId(int $topicSubSectionId)
    {
        //buildPage__METHOD__,"topicSubSectionId=$topicSubSectionId",debug_backtrace()[1]['function']);
        $this->tbl="lesson";
        $this->wc="topic_subsection_id=$topicSubSectionId";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }
    private function setLessonLcde()
    {
        //buildPage__METHOD__);
        $this->dbBase=$this->dbMaths;
        $this->tbl="lesson";
        $this->wc="(status = 1) AND (lcde = 'UnKnown')"; // published nut lcde not set
        $this->limit="LIMIT 3";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(count($this->recordSetArray)==0){
            return;
        }
        for ($i = 0; $i < count($this->recordSetArray); $i++) {
            $this->tbl="lesson";
            $id=$this->recordSetArray[$i]['id'];
            $lcde=md5($id);
            $this->wc="id = $id";
            $this->updates="lcde='$lcde'";
            parent::connectDatabase();
            parent::sqlUpdate();
        }
        return;
    }
    public function getLessonByLcde(string $lcde)
    {
        //buildPage__METHOD__,$lcde);
        $this->setLessonLcde();
        $this->tbl="lesson";
        $this->wc="lcde='$lcde'";
        $this->limit="LIMIT 1";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }    
    public function getLessonById(int $lessonId)
    {
        $this->tbl="lesson";
        $this->wc="id=$lessonId";
        $this->limit="LIMIT 1";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            $retArray=$this->flattenArray($this->recordSetArray);
            if($retArray['lcde']=="UnKnown"){
                $this->setLcdeOnLesson($retArray['id']);
                $retArray['lcde']=md5($retArray['id']);
            }
            return $retArray;
        }
        return null;
    }
    private function setLcdeOnLesson($id)
    {
        $md=md5($id);
        $this->tbl="lesson";
        $this->wc="id=$id";
        $this->updates="lcde = '$md'";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlUpdate();
        return;
    }

    public function getSubSection(int $subSectId)
    {
        //buildPage__METHOD__,$subSectId);
        $this->tbl="topic_subsection";
        $this->wc="id=$subSectId";
        $this->limit="LIMIT 1";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }
    public function getSubTopicById(int $id)
    {
        //buildPage__METHOD__,$id);
        $this->tbl="topic_subsection";
        $this->wc="id=$id";
        $this->limit="LIMIT 1";
        $this->dbBase=getenv('dbMaths');;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;     
    }
    public function getAllSubTopicsByTopicId(int $subSectId)
    {
        //buildPage__METHOD__,$subSectId);
        $this->tbl="topic_subsection";
        $this->wc="topic_id=$subSectId";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
    }
    public function getAllTopicsBySubjectId(int $subjectId, int $sts=1000)
    {
        //buildPage__METHOD__,$subjectId,$sts);
        $stsTxt="= $sts";
        if($sts==1000){
            $stsTxt="> 0";
        }
        $this->tbl="topic";
        $this->wc="(subject_id=$subjectId) AND (status $stsTxt)";
        $this->dbBase=$this->dbMaths;        
        $this->tbl="topic";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
    }
    public function getTopicsBySubjectId(int $subjectId)
    {
        //buildPage__METHOD__,$subjectId);
        $this->tbl="topic";
        $this->wc="(subject_id=$subjectId) AND (status > 0)";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }
    private function regenSession()
    {   //2101  -08 
        //buildPage__METHOD__,"Null",debug_backtrace()[1]['function']);
        $_SESSION['oldSessionId']=$_SESSION['sessionId'];
        if(isset($_SESSION['us'][1])){
            unset($_SESSION['us']);
        }
        $sessVars=$_SESSION;
        // Call session_create_id() while session is active to 
        // make sure collision free.
        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }
        // WARNING: Never use confidential strings for prefix!
        $newSid = substr(bin2hex(md5(rand(10000,100000000))),-32) ;
        // Set deleted timestamp. Session data must not be deleted immediately for reasons.
        $_SESSION['deleted_time'] = time();
        // Finish session
        session_commit();
        // Make sure to accept user defined session ID
        // NOTE: You must enable use_strict_mode for normal operations.
        // Set new custom session ID
        session_id($newSid);
        // Start with custom session ID
        ini_set('session.use_strict_mode', 1);
        session_start();
        unset($sessVars['wwwwww']);
        unset($sessVars['sessionId']);
        $_SESSION['sessionId']=session_id();
        $a=array_merge($_SESSION,$sessVars);
        $_SESSION=$a;
        $_SESSION['at']['regenat']=time();
    }
    public function handleSession()
    {   //2101  -08 
        //buildPage__METHOD__,"Null",debug_backtrace()[1]['function']);
        $_SESSION['at']['life']=getenv("sessionLife");
        unset($_SESSION['at']['r']);
        unset($_SESSION['liveUntil']);
        if(!isset($_SESSION['sessionId'])){$_SESSION['sessionId']=session_id();}
        if(!isset($_SESSION['at']['last'])){$_SESSION['at']['last']=time();}
        $_SESSION['at']['now']=time();
        $_SESSION['at']['waslast']=$_SESSION['at']['last'];
        $_SESSION['at']['elapsed']=time()-$_SESSION['at']['last'];
        if($_SESSION['at']['elapsed']>$_SESSION['at']['life']){
            $_SESSION['at']['re-call'][]=time();
            $this->regenSession();
        }
        $_SESSION['at']['last']=time();
        if(isset($_SESSION['us'])){
            if(isset($_SESSION['us'][3])){
                $cde=$this->deEncrypt($_SESSION['us'][3]);
                $result=$this->testMD5Number($cde);
                if($result==1){
                    $this->logInOutMenuTxt="<li><a href=\"logout.php\" class=\"button fit\">Log Out</a></li>";
                    $cde=$this->deEncrypt($_SESSION['us'][1]);
                    $this->currentUserArray=$this->getUserByEmailCode($cde);
                    if(isset($this->currentUserArray['id'])){
                        $_SESSION['us'][$this->currentUserArray['id']]=$this->enCryp($this->currentUserArray['sys_check']);
                    }
                }
            }
        }
        $this->buildUserAddedMenus();
    }
    private function buildUserAddedMenus()
    {
        //2101  -22
        //buildPage__METHOD__,"Null",debug_backtrace()[1]['function']);
        $tmpLogInVar="logOut.php";
        $tmpLogInText="Log out &amp Restarat";
        if(!isset($this->currentUserArray)){
            $tmpLogInVar="logIn.php";
            $tmpLogInText="Log In or Register";
        }
        $this->addedMenu.="
            <li>    
              <a href=\"$tmpLogInVar\" class=\"button fit\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">$tmpLogInText</a>
            </li>";
        //$this->profileUserArray=$this->getUserProfiles($this->currentUserArray['id']);
        // if(is_null($this->profileUserArray)){ 
        //     return;
        // }
        // $this->menuAddedArray=array();
        // for ($i = 0; $i < count($this->profileUserArray); $i++) {
        //     $this->menuAddedArray[$i]=$this->getUrlsByProfile($this->profileUserArray[$i]['profile_id']);
        // }
        // $noOfAddedMenuItemsRBAC=count($this->menuAddedArray);
        // for($m=0;$m<$noOfAddedMenuItemsRBAC;$m++){
        //     $this->addedMenu.="<li><a href=\"{$this->menuAddedArray[$m]['urlName']}\">{$this->menuAddedArray[$m]['salutation']}</a>xxx</li>";
        // }
        return;
    }

    public function padText(string $string, int $lenght=20)
    {
        //buildPage__METHOD__,"$string , $lenght");
        $tmpStr="";
        for ($i = 0; $i < $lenght; $i++) {
            $tmpStr.="&nbsp;";
        }
        return $string.$tmpStr;
    }
    public function getIntegratedSubjects(int $topicFilter)
    {
        //buildPage__METHOD__,$topicFilter);
        $retArray=$this->getAllSubjects();
        for ($i = 0; $i < count($retArray); $i++) {
            $this->getAllTopicsBySubjectId($retArray[$i]['id'],$topicFilter);
            $retArray[$i]['topicsArray']=$this->recordSetArray;
            for ($j = 0; $j < count($retArray[$i]['topicsArray']); $j++) {
                $this->getAllSubTopicsByTopicId($retArray[$i]['topicsArray'][$j]['id']);
                $retArray[$i]['topicsArray'][$j]['subTopicsArray']=$this->recordSetArray;
            }
        }
        return $retArray;
    }
    public function setToken(int $userId, int $type=1, int $days=1, string $token=null)     //2101   23
    {
        //waynep
        //buildPage__METHOD__,"userId=$userId|type=$type|days=$days|token=$token",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbAppControl;
        switch ($type) {
            case 1:
                //login
                $typeUse="loginPin";
                $expireDays=5;
                break;
            case 20:
                // full
                $typeUse="expires|1|Full|Use";
                $expireDays=1;
                break;
            default:
                //unKLnown profile
                $typeUse="UnKnown";
                $expireDays=-1;
                break;
        }
        if(is_null($token)){
            $token=substr(md5($type),rand(0,24),6); 
        }
        $expires=time()+($expireDays*24*60*60);
        $expires=date("Y-m-d H:i:s",$expires);
        $created=date("Y-m-d H:i:s");
        parent::connectDatabase();
        $this->tbl='tokens';
        $this->inserts="
            token_type_id=$type,
            usage_count=0,
            type_use='$typeUse',
            user_id=$userId,
            token='$token',
            created='$created',
            expires ='$expires',
            status = 1;
        ";
        parent::sqlInsert();
    }
    public function getToken(int $userId, int $type=1)          //  2101    23
    {
        //waynep
        //buildPage__METHOD__,"UserId=$userId|type=$type",debug_backtrace()[1]['function']);
        $this->unsetExpiredTokens();    //ok
        $this->dbBase=$this->dbAppControl;
        $this->tbl="tokens";
        $this->orderBy="id DESC";
        $this->wc="(user_id=$userId) AND (token_type_id = $type) AND (status = 1)";
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        $this->sqlSetRecordset();
        if(count($this->recordSetArray)==0){
            return null;
        }
        return $this->flattenArray($this->recordSetArray);    
    }
    public function setSafeEmail($email)
    {
        //buildPage__METHOD__,$email);
        $email=str_replace(".", "[dot]",$email);
        $email=str_replace("@", "[at]",$email);
        return $email;
    }
    public function register(string $email)
    {
        //buildPage__METHOD__,$email);
        $userRegArray=$this->getUserByEmail($email);
        If(!is_null($userRegArray)) {
            if($userRegArray['status']==-100){
                $this->sendUserRegMail($email);
            }
            return $userRegArray;    
        }
        $this->setRegUser($email);
        $userArray=$this->getUserByEmail($email);     
        if(is_null($userArray)){
            $_SESSION['errs']['page']="Register with us";
            $_SESSION['errs']['detail']="We are not able to register the address $email";
            header("Location: errors.php");
            exit();
        }   
        $this->sendUserRegMail($email);   
        
        return $userArray;    
    }
    private function logLessonRequest(int $userId, int $lessonId)
    {
        //buildPage__METHOD__,"Implicit :: {$_SESSION['us'][1]}");   
        $this->dbBase=$this->dbAppControl;
        $this->tbl="user_requests";
        $this->wc="(request_type=1) AND (request_link_id=$lessonId) AND (user_id=$userId)";
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        $this->sqlSetRecordset();
        if(count($this->recordSetArray)==1){
            return "(Done on {$this->recordSetArray[0]['created']})";
        }
        $this->tbl="user_requests";
        $this->inserts="
            request_type=1,
            request_link_id=$lessonId, 
            user_id=$userId
        ";
        $this->dbBase=$this->dbAppControl;
        parent::connectDatabase();
        parent::sqlInsert();
        return null;
    }
    public function sendRequestLessonMail(int $lessonId)
    {
        //buildPage__METHOD__,"Implicit :: {$_SESSION['us'][1]}");   
        $emailCode=$this->deEncrypt($_SESSION['us'][1]);
        $userArray=$this->getUserByEmailCode($emailCode);
        $lessonArray=$this->getLessonById($lessonId);
        $toAddArray=array($userArray['email']);
        $nme=$userArray['userid'];
        if(strlen($userArray['full_name'])>0)
        {
            $nme=$userArray['full_name'];
        }
        $done=$this->logLessonRequest($userArray['id'],$lessonArray['id']);
        $doneTxt="";
        if(!is_null($done)){
            $doneTxt=$done;
        }
        $subject="Math4YouToday Requested Lesson";
        $body="
            Dear $nme,
            <br><br>
            Thank you for requesting this lesson. $doneTxt
            <br><br>
            Lesson: <b>{$lessonArray['lesson']}</b>
            <br><br>
            Explained as: {$lessonArray['explained']}
            <br><br>
            We will schedule and detrmine the priority of this activity.
            <br><br>
            You may see your requests on the Profiles Page of https://maths4you.today
            <br><br>
            Regards,
            <br>
            <br>
            https://Math4You.today
            <br>";
            return $this->sendMail($toAddArray,$subject,$body,$this->myName);        
        exit();
    }
    public function sendUserRegMail(string $email)
    {
        //buildPage__METHOD__,$email);;
        $encryptedData=$this->enCryp($email);
        $regEndPoint=getenv("domain")."/blindEndPoint.php?email=$encryptedData&end=1";
        $lnk="<a href=\"$regEndPoint\">Validate Your e-Mail address</a>";
        $toAddArray=array($email);
        $subject="Math4You Registration";
        $body="
            Welcome,
            <br><br>
            Thank you for registering with us.
            <br><br>
            To Complete this process simply click on this link:
            <br><br>
            $lnk
            <br><br>
            This will validate your email address and send you a free Usage Token.
            <br><br>
            Regards from,
            <br>
            <br>
            https://Math4You.today
            <br>";
            return $this->sendMail($toAddArray,$subject,$body,$this->myName);
    }
    public function sendUserLoginToken(string $email)          // 2101     -23
    {   //waynep
        //buildPage__METHOD__,$email,debug_backtrace()[1]['function']);
        $tokenArray=$this->getToken($this->currentUserArray['id'],1); //ok
        if(is_null($tokenArray)){
            $this->setToken($this->currentUserArray['id'],1,5); //ok
        }
        $tokenArray=$this->getToken($this->currentUserArray['id'],1); //ok
        $toAddArray=array($email);
        $from=getenv("domain");
        $subject="Team C Login Token";
        $body="
            Welcome,
            <br><br>
            This is your login token. Copy and Paste it into the screen where you login:
            <br><br>
            Token: <b>{$tokenArray['token']}</b>
            <br><br>
            Token will Expire at <b>{$tokenArray['expires']}</b>
            <br><br>
            Regards,
            <br>
            <br>
            $from
            <br>";
            return $this->sendMail($toAddArray,$subject,$body,$this->myName);
    }    
    public function deEncrypt(string $data)
    {
        //2101  -07 
        //buildPage__METHOD__,"toDecrypt=$data",debug_backtrace()[1]['function']);
        for ($i = 1000; $i < 1010; $i++) {
            $extract=md5($i);
            $data=str_replace($extract,"",$data);
        }
        return $data;        
    }
    public function testMD5Number(string $code)
    {
        //2101  -07 
        //buildPage__METHOD__,"test=$code",debug_backtrace()[1]['function']);
        for ($i = -100; $i < 100 ; $i++) {
            $test=md5($i);
            if($test==$code){
                return $i;
            }
        }
        return null;
    }
    public function randomiseUserLoginSettings(string $mcde, int $loginSts)
    {
        //2101  -07 
        //buildPage__METHOD__,"test=$mcde::loginSts=$loginSts",debug_backtrace()[1]['function']);
        $user=$this->getUserByEmailCode($mcde);
        if(is_null($user)){
            return Null;
        }
        $retArray[1]=$this->enCryp($user['email']);
        $retArray[2]=$this->enCryp($user['status']); 
        $retArray[3]=$this->enCryp($loginSts); 
        return $retArray;
    }
    public function enCryp(string $data)
    {
        //2101  -07 
        //buildPage__METHOD__,"encypt=$data",debug_backtrace()[1]['function']);
        $randPos1=rand(1000,1007);
        $randPos2=$randPos1+1;
        $randPos3=$randPos1+2;
        $x=rand(3,5);
        if(($x % 2) ==0)
        {
            return md5($randPos2).md5($data).md5($randPos3); 
        }
        if(($x % 3) ==0)
        {
            return md5($randPos3).md5($randPos1).md5($data); 
        }        
        if(($x % 5) ==0)
        {
            return md5($randPos2).md5($randPos3).md5($data).md5($randPos1); 
        }
        return md5($randPos3).md5($randPos1).md5($data).md5($randPos2); 
    }
    public function setRegUser($email)
    { 
        //2101  -07
        //buildPage__METHOD__,"email=$email",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbAppControl;
        $SysCheckTime=time();
        $this->tbl='users';
        $md=md5($email);
        $this->inserts="
            userid='$email',
            mcde='$md',
            email='$email',
            sys_check=$SysCheckTime,
            password='onLine1',
            is_email_verified =0,
            login_attempts=-1,
            logons =-1,
            devolved_from_id=-99,
            cell_token='pending',
            status=-100
        ";
        parent::sqlInsert();
    }
    private function getUrlsByProfile(int $profileId){
        //2101  -07 
        //buildPage__METHOD__,"Profileid=$profileId",debug_backtrace()[1]['function']);
        $this->tbl="profiles_urls";
        $this->wc="(profile_id=$profileId) AND (status=1)";
        $this->orderBy="control_id ASC";
        $this->limit="LIMIT 1";
        $this->dbBase=$this->dbAppControl;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            $retArray=$this->flattenArray($this->recordSetArray);
            $urlArray=$this->getUrlById($retArray['url_id']);
            $retArray['urlName']=$urlArray['name'];
            $retArray['salutation']=$urlArray['salutation'];
            return $retArray;
        }
        return null;     

    }
    private function getUrlById(int $urlId)
    {
        //2101  -07   
        //buildPage__METHOD__,"urlId=$urlId",debug_backtrace()[1]['function']);
        $this->tbl="urls";
        $this->wc="id=$urlId";
        $this->dbBase=$this->dbAppControl;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }
    
    public function getIntegratedSubTopics(int $subTopicId)
    {
        //buildPage__METHOD__,$subTopicId);
        $subTopicArray=$this->getSubTopicById($subTopicId);
        $lessonArray=$this->getLessonBySubSectionId($subTopicArray['id']);
        $subTopicArray['lessonArray']=$lessonArray;
        return $subTopicArray;        
    }
/* 
$this->pmeVarsArray['fdd'][$fld]['values2']['0']='Inactive';
$this->pmeVarsArray['fdd'][$fld]['values2']['1']='Published';
$this->pmeVarsArray['fdd'][$fld]['values2']['2']='Pending Publish';
$this->pmeVarsArray['fdd'][$fld]['values2']['5']='Active Edit';
$this->pmeVarsArray['fdd'][$fld]['values2']['9']='Imported';
$this->pmeVarsArray['fdd'][$fld]['values2']['-9']='Hidden';
*/

    public function getAccessCode()
    {
        //buildPage__METHOD__);
        // assumes that the Session us is set
        $retVal="Public";
        if(isset($_SESSION['us'][20])){
            return "Full";
        }
        $this->currentUserArray=$this->getUserFromSession($_SESSION['us'][1]);
        $this->setAuthText();
        if(($this->currentUserArray['accessLevel']=="Full") OR ($this->currentUserArray['accessLevel']=="Premium")){
            return $this->currentUserArray['accessLevel'];
        }
        $this->getTokensByUserId($this->currentUserArray['id'],20);
        $this->currentTokenArray=$this->recordSetArray;
        echo("<br>Array:params(".__LINE__."({$this->myName}))<br><pre>"); print_r($this->currentTokenArray); echo("</pre><hr>");
        if(count($this->currentTokenArray)==0){
            //I.e. no tokens yet
            return null;
        }
        if(count($this->currentTokenArray)>5){
            return "ToManyTokens";
        }
        if(isset($this->currentTokenArray[0])){
            if($this->currentTokenArray[0]['status']==1){
                // still active
                $encrValue=$this->enCryp($this->currentTokenArray['token']);
            }
            if(count($this->currentTokenArray)<6){
                // now a token must be re-issued
                $tkn=substr(md5("WaynePhilip"),rand(0,22),8);
                $this->setToken($this->currentUserArray['id'],20,1,md5(time()));
                $encrValue=$this->enCryp($tkn);       
            }
            $_SESSION['us']['20']=$encrValue;   
            return "Full";     
        }
    }
    public function setChargeCode(int $cde)
    {
        //buildPage__METHOD__,$cde);
        switch ($cde) {
            case 1:
                return "Basic|USD 5 monthly ";
                break;
            case 2:
                return "Full|USD 20 monthly ";
                break;
            case 2:
                return "Premium|USD 50 monthly";
                break;                              
            default:
            return "Public|Available to all (Free)";
                break;
        }
    }
    public function setStratusTxt(int $sts)
    {
        //buildPage__METHOD__,$sts);
        switch ($sts) {
            case 0:
                return "InActive|No longer Active";
                break;
            case 1:
                return "Published|Media &amp; Quizzes Available";
                break;
            case 2:
                return "Imminent|About to Publish &rarr; Come Back Soon";
                break;
            case 5:
                return "Editing|Media actively being created";
                break;
            default:
                return "Evaluating|Media released to testers";
                break;
        }
    }
    public function setAuthText(string $retType="pl")
    {  
        //buildPage__METHOD__);
        $ret="You don't have access to <b>protected</b> content. <a href=\"login.php\" class=\"button special small\">Log in or Register</a>";
        if(isset($_SESSION['us'])){   // logged in
            $email=$this->deEncrypt($_SESSION['us'][1]);
            $this->currentUserArray=$this->getUserByEmailCode($email);
            $this->currentUserArray['accessLevelM4U']="Public";
            switch ($this->currentUserArray['status']) {
                case 50:
                    $this->currentUserArray['accessLevel']="Premium";
                    break;
                case 20:
                    $this->currentUserArray['accessLevel']="Full";
                    break;
                case 5:
                    $this->currentUserArray['accessLevel']="Basic";
                    break;
                default:
                $this->currentUserArray['accessLevel']="Public";
            }
            $ret="Welcome <span class=\"orangeColor\">{$this->currentUserArray['first_name']}</span>, your access level has been set as <span class=\"orangeColor\">{$this->currentUserArray['accessLevel']}</span>";
        }
        return $ret;
    }
    public function getSubTopicsArray(int $topicId)
    {
        $this->tbl="topic_subsection";
        $this->wc="(topic_id=$topicId) and (status>0)";
        $this->orderBy="seq ASC";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;        
    }
    public function flattenArray(array $array, int $recNum=0) {             //2101          24          
        //buildPage__METHOD__,"array[0]",debug_backtrace()[1]['function']);
        $retArray=array();
        foreach ($array[$recNum] as $key => $value) {
            $retArray[$key]=$value;
        }
        return $retArray;
    }
    public function getSubject(int $subjectId)
    {
        //buildPage__METHOD__,"subjectId=$subjectId",debug_backtrace()[1]['function']);
        $this->tbl="subjects";
        $this->wc="(id=$subjectId) and (status=1)";
        $this->limit="LIMIT 1";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }
    public function getAllSubjects()
    {
        //buildPage__METHOD__);
        $this->tbl="subjects";
        $this->wc="status > -1";
        $this->orderBy="seq ASC";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }    
    public function getAllTopics()
    {
        //buildPage__METHOD__);
        $this->tbl="topic";
        $this->orderBy="seq ASC";
        $this->wc="status=1";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }  
    public function getTopicById(int $id)
    {
        //buildPage__METHOD__,$id);
        $this->tbl="topic";
        $this->orderBy="seq ASC";
        $this->wc="id = $id";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(count($this->recordSetArray)>0){
            $returnArray=$this->flattenArray($this->recordSetArray);        
        }
        return $returnArray;
    }  
    public function getSubjectById(int $subjId)
    {
        //buildPage__METHOD__,$subjId);
        $this->dbBase=$this->dbMaths;
        $this->tbl="subjects";
        $this->wc="id=$subjId";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        $returnArray=null;
        if(count($this->recordSetArray)>0){
            $returnArray=$this->flattenArray($this->recordSetArray);        
        }
        return $returnArray;
    }
    public function setMenuData()
    {
        //buildPage__METHOD__);
        $this->tbl="topic";
        $this->orderBy="seq ASC";
        $this->wc="status > 0";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        $topicsArray=$this->recordSetArray;
        for ($i = 0; $i < count($topicsArray); $i++) {
            $subjId=$topicsArray[$i]['subject_id'];                   /// yes this is corect (dbErr=topic_id shouild be Subject Id)
            $subjArray=$this->getSubjectById($subjId);
            $subjArray=$this->flattenArray($subjArray);
            $topicsArray[$i]['subject_id']=$subjArray['id'];
            $topicsArray[$i]['subject_seq']=$subjArray['seq'];
            $topicsArray[$i]['subject_menu_txt']=$subjArray['menu_txt'];
            $sts="InProgress";
            $updTxt="Imminent";
            if($topicsArray[$i]['status']==1){
                $sts="Published";
                $updTxt=substr($topicsArray[$i]['updated'],0,10);
            }
            $topicsArray[$i]['statusTxt']=$sts;
            $topicsArray[$i]['updatedTxt']=$updTxt;
        }  
        return $topicsArray;
    }  
    public function getTopic(int $topicId)
    {
        //buildPage__METHOD__,$topicId);
        $this->dbBase=$this->dbMaths;
        $this->tbl="topic";
        $this->wc="(id=$topicId)";
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }
    public function sendMail(array $addrTo, string $subject, string $bodyHT, string $caller, $pdfStream=null,$pdfDocName=null)  //2101      24
    {   //waynep1
        //buildPage__METHOD__,"VariousInArray",debug_backtrace()[1]['function']);
        $sendArray=array();
        $addrTo[]=$this->adminMail;
        $bodyHT="<img src=\"https://tetradime.com/favicon.png\"><br><br>".$bodyHT;
        $sendArray['to']=$addrTo;
        $sendArray['subject']=$subject;
        $sendArray['body']=$bodyHT;
        if(!is_null($pdfStream)){
            $sendArray['pdfStream']=$pdfStream;
            $sendArray['pdfDocName']=$pdfDocName;
        }
        $obj=new sendMailNow;
        return $obj->sendMail($sendArray);
    }
    public function getLessonsForSubTopicId(int $subSectionId)
    {
        $this->traceObj(__METHOD__,"subSection=$subSectionId");
        $this->tbl="lesson";
        $this->orderBy="seq ASC";
        $this->wc="(topic_subsection_id=$subSectionId) AND (status>0)";
        $this->dbBase=$this->dbMaths;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }   
    private function unSetSessionVars()             //2101      24
    {  //waynep
        //buildPage__METHOD__,"Intrinsic({$this->pageArray['page']['session_resets']})",debug_backtrace()[1]['function']);     
        $varsArray=explode("|",$this->pageArray['page']['session_resets']);
        foreach ($varsArray as $key => $value) {
            if(isset($_SESSION[$value])) {
                unset($_SESSION[$value]);
            }
        }
    }
    public function getAllActiveLessonsSortedDesc()
    {
        //buildPage__METHOD__);
        $this->dbBase=$this->dbMaths;
        $this->tbl='lesson';
        $this->wc="(status = 1)";
        $this->orderBy="updated DESC";
        parent::connectDatabase();
        parent::sqlSetRecordset();            
        $retArray=$this->recordSetArray;
        for ($l = 0; $l < count($retArray); $l++) {
            $subSectArray=$this->getSubSection($retArray[$l]['topic_subsection_id']);
            $retArray[$l]['subSectionTxt']=$subSectArray['sub_section'];
            $retArray[$l]['topicId']=$subSectArray['topic_id'];
        }
        for ($l = 0; $l < count($retArray); $l++) {
            $topicArray=$this->getTopicById($retArray[$l]['topicId']);
            $retArray[$l]['topicTxt']=$topicArray['topic'];
        }        
        return $retArray;
    }
    private function getUserProfiles(int $userId)
    {
        //2101  -07          
        //buildPage__METHOD__,"userId=$userId(Terminated)",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbAppControl;
        $this->tbl="profiles_users";
        $this->wc="(user_id = $userId) AND (status=1)";
        parent::connectDatabase();
        parent::sqlSetRecordset();  
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }
    public function getAllLessonsByStatus(int $status)
    {
        //2101  -07          
        //buildPage__METHOD__,"status=$status",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbMaths;
        $this->tbl="lesson";
        $this->wc="status = $status";
        parent::connectDatabase();
        parent::sqlSetRecordset();  
        if(isset($this->recordSetArray[0])){
            return $this->recordSetArray;
        }
        return null;
    }
    public function getLesson(int $lessonNo)
    {
        //2101  -07          
        //buildPage__METHOD__,"lessonId=$lessonNo",debug_backtrace()[1]['function']);
        $this->dbBase=$this->dbMaths;
        $this->tbl="lesson";
        $this->wc="id=$lessonNo";
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        parent::sqlSetRecordset();  
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }
    public function setLoginIncrement()
    {
        //buildPage__METHOD__);
        $ddtt=date("Y-m-d H:i:s");
        $logons=$this->params['user_currrent_array']['logons']+1;
        $id=$this->params['user_currrent_array']['id'];
        $this->tbl="users";
        $this->wc="id=$id";
        $this->updates="logons=$logons, last_login_date='$ddtt' ";
        $this->dbBase=$this->dbAppControl."_".$_SESSION['invocation_db_suffix'];
        parent::connectDatabase();
        $this->sqlUpdate();
    }
    private function getUserByUserName($userName)
    {
        //buildPage__METHOD__,$userName);
        $this->userCurrentArray=Array();
        $this->dbBase=$this->dbAppControl;
        $this->tbl="users";
        $this->wc="userid='$userName'";
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }
    private function getUserByEmail(string $email)          
    {
        //buildPage__METHOD__,$email,debug_backtrace()[1]['function']);
        $this->currentUserArray=null;
        $this->dbBase=$this->dbAppControl;
        $this->tbl="users";
        $this->wc="email='$email'";
        $this->limit="LIMIT 1";
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }
    public function setUserActivity(array $data)            //2101      25
    {   
        $user_id=0;
        $this->currentUserArray=null;
        if(!isset($_SESSION['us'])){
            $this->currentUserArray=$this->getUserByEmail($data['post_email']);
        }
        if(isset($_SESSION['us'])){
            $this->currentUserArray=$this->getUserByUsSess();
        }
        if(isset($this->currentUserArray['id'])){
            //echo("<br>Array:params(".__LINE__."({$this->myName}))<br><pre>"); print_r($this->currentUserArray); echo("</pre><hr>");
            $user_id=$this->currentUserArray['id'];
        }
        if(!isset($data['post_Name'])){$data['post_Name']="UnKnown";}
        if(!isset($data['post_TopicSuggested'])){$data['post_TopicSuggested']="NoTopic";}
        if(!isset($data['post_Comments'])){$data['post_Comments']="NoComments";}
        if(!isset($data['post_stars'])){$data['post_stars']=0;}
        if(!isset($data['post_Token'])){$data['post_Token']='off';}
        if(!isset($data['post_NewsLetter'])){$data['post_NewsLetter']='off';}
        $message="{$data['post_TopicSuggested']}::{$data['post_Comments']}";
        $captureName=$data['post_Name'];
        $email=$data['post_email'];
        $token_request=$data['post_Token'];
        $news_request=$data['post_NewsLetter'];              
        $stars=$data['post_stars'];
        $origin=$data['post_source'];
        $ip=$_SERVER['REMOTE_ADDR'];
        $sid=session_id();
        $this->inserts="
            ip='$ip',
            session_id='$sid',
            capture_name='$captureName',
            email='$email',
            token_request='$token_request',
            news_request='$news_request',
            stars=$stars,
            user_id=$user_id,
            origin='$origin',
            message='$message',
            response='Acknowledged',
            status=0";
        $this->tbl="user_activity";
        $this->dbBase=$this->dbAppControl;           
        parent::connectDatabase();
        parent::sqlInsert();
        if(is_null($this->currentUserArray)){
            return null;
        }
        return $this->currentUserArray;
    }
    public function getUserByUsSess()                   //2101          25
    {
        //buildPage__METHOD__,"Interinsic(session-us-0)",debug_backtrace()[1]['function']);
        $emlCode=$this->deEncrypt($_SESSION['us'][0]);
        $this->tbl="users";
        $this->wc="mcde='$emlCode'";
        $this->limit="LIMIT 1";
        $this->dbBase=$this->dbAppControl;
        parent::connectDatabase();
        parent::sqlSetRecordset();
        if(isset($this->recordSetArray[0])){
            return $this->flattenArray($this->recordSetArray);
        }
        return null;
    }
    public function debugPrint(string $text, array $array)
    {
        //buildPage__METHOD__,"text=$text | and an array");
        $file="00-debug.txt";
        $contents=date("Y-m-d H:i:s")."==>$text\n================\n";
        $contents.=print_r($array,true);
        if(file_exists($file)){
            $contents.="\n\n".file_get_contents($file);   
        }
        file_put_contents($file,$contents);
    }
}
