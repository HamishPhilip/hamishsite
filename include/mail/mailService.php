<?php namespace mailService;
if (session_status() == PHP_SESSION_NONE) {
    file_put_contents("errlog","\n".date("Ymd H:i:s")."Violation::No session for".__FILE__,FILE_APPEND);
    exit();
}
$fl="{$_SERVER['DOCUMENT_ROOT']}/include/mail/PHPMailer/src/PHPMailer.php";
include_once($fl);

use PHPMailer\PHPMailer\PHPMailer as Sender;

class Send 
{
    private $myName="MailService";
    public function sendMail(array $sendArray)
    {
        $mail = new Sender;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = getenv('smtpServer');
        $mail->Port = getenv('smptpPort');
        $mail->SMTPSecure = getenv('smtpSecure');
        $mail->SMTPAuth = getenv('smtpAuth');
        $mail->Username = getenv('smtpUser');
        $mail->Password = getenv('smtpPassword');
        $nameFrom=getenv('mailFrom');
        if (isset($sendArray['from_person'])) {
            $nameFrom=$sendArray['from_person'];
        }
        if(isset($sendArray['pdfStream'])){
            $mail->AddStringAttachment($sendArray['pdfStream'], $sendArray['pdfDocName'], 'base64', 'application/pdf');
        }
        $mail->setFrom(getenv('smtpUser'), $nameFrom);
        $mail->addReplyTo(getenv('smtpUser'), $nameFrom);
        $nameTo="";
        if (isset($sendArray['to_person'])) {
            $nameTo=$sendArray['to_person'];
        }
        $mail->addAddress($sendArray['to'][0], $nameTo);
        $arr=$sendArray['to'];
        foreach ($arr as $key => $value) {
            $mail->addAddress($value, $nameTo);
        }
        $renderAt=date("Y-m-d H:i:s");
        $body=$sendArray['body']."<br><br><br>Sent at:<b> $renderAt </b><br>";
        $mail->msgHTML($body);
        $mail->Subject=$sendArray['subject'];
        $mail->msgHTML(urldecode($sendArray['body']));
        if(!$mail->send()) {
            $sendArray['sendStatus']=0;
            $sendArray['sendErrors']="Sending Error {$mail->ErrorInfo}";
            $sendArray['sendErrorsDetail']=$mail->ErrorInfo;
            $this->writeLog($sendArray);
            return $sendArray;
        }
        $sendArray['sendStatus']=1;
        $this->writeLog($sendArray);
        return $sendArray;
    }
    private function writeLog(array $sendArray)
    {
        $dir=$_SERVER['DOCUMENT_ROOT'];
        $sendArray['mail_ip']=$_SERVER['SERVER_ADDR'];
        $file="$dir/.errorLogMail";
        if($sendArray['sendStatus']){
            $file="$dir/.logMail";
        }
        $contents="";
        if(file_exists($file)){
            $contents.=file_get_contents($file);
            unlink($file);
        }
        $contents.="\n\n--Mail @ ".date('Y-m-d H:i:s')."\n";
        $contents.=print_r($sendArray,true);
        file_put_contents($file, $contents);
    }
}
