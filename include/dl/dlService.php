<?php namespace dataLogic;

if (session_status() == PHP_SESSION_NONE) {
    file_put_contents(".errlog","\n".date("Ymd H:i:s")."Violation::No session for".__FILE__,FILE_APPEND);
    exit();
}
use PDO;
use PDOException;

class DlService
{
    /*   Privates   */
    public $deBug=1;
    private $myName="DlService";
    private $dbh;
    /** public */
    public $dbType="mysql"; // as used by $dsn
    public $dbBase;
    public $dbHost;
    public $dbUser;
    public $dbPwd;
    public $dbAppControl;
    public $dbWebsite;
    public $disConnect=true;
    /*std databassMasters*/
    public $logsTo;
    public $dbMaths;
    /* old */
    public $updates;
    public $dbObj;
    public $dbPort=3306;
    public $tbl;
    public $wc;
    public $distinctFieldsArray=[];
    public $inserts;
    public $orderBy;
    public $limit;
    public $sql;
    public $result;
    public $recordCount;
    public $recordSetArray;
    public $rec;
    public $sqlRowsAffected;
    public $sqlWhereClause;
    public $r;
    public $sqlError;
    public $sqlLastId;
    public $decode=array();
    public $params=array();
    public $sessionLife;
    public function __construct()
    {
        $this->dbHost=getenv("dbHost");
        $this->dbUser=getenv("dbUser");
        $this->dbPwd=getenv("dbPwd");
        $this->dbMaths=getenv("dbMaths");
        $this->dbAppControl=getenv("dbAppControl");
        $this->dbWebsite=getenv("dbWebsites");
        $this->deBug=getenv("debug");
        $this->sessionLife=getenv("sessionLife");
        
    }
    public function connectDatabase()
    {
        if($this->disConnect){
            if (is_object($this->dbObj)) {
                $this->dbObj=null;
            }
        }
        $this->dbh="mysql:host={$this->dbHost};dbname=".$this->dbBase;
        try {
            $this->dbObj = new PDO($this->dbh, $this->dbUser, $this->dbPwd);
        } catch (PDOException $e) {
            echo("<br>Connection failed:".__LINE__."::[{$this->myName}]::in::".__METHOD__."::errr::". $e->getMessage());
            echo("<br>{$this->dbh}<br>"); 
            exit();
        }        
    }
    public function buildGetSql()
    {
        $this->sql="SELECT * FROM {$this->tbl} ";
        if (count($this->distinctFieldsArray)>0) {
            $flds=implode(",", $this->distinctFieldsArray);
            $this->sql="SELECT DISTINCT $flds FROM {$this->tbl}";
        }
        if (isset($this->wc)) {
            $this->sql.= "WHERE ({$this->wc}) ";
        }
        if (isset($this->orderBy)) {
            $this->sql.= "ORDER BY {$this->orderBy} ";
        }
        if (isset($this->limit)) {
            $this->sql.= "{$this->limit}";
        }
        $this->reSetSqlParams();
    }
    public function sqlDelete()
    {
        $this->sql="DELETE FROM {$this->tbl} WHERE ({$this->wc}) ";
        $this->dbObj->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->dbObj->exec($this->sql);
        $strSql=substr($this->sql,0,12)."..";
    }
    public function sqlInsert() 
    {
        $this->sql="INSERT INTO {$this->tbl} SET {$this->inserts} ";
        $strSql=substr($this->sql,0,12)."..";
        $sth = $this->dbObj->prepare($this->sql);
        $sth->execute();
        $this->sqlLastId=$this->dbObj->lastInsertId();
        $this->reSetSqlParams();
        unset($this->sql);
    }
    public function sqlBlindExecute()
    {
        $sth = $this->dbObj->prepare($this->sql);
        $sth->execute();
        $this->sqlLastId = $sth->rowCount();
         unset($this->sql);
    }    
    public function sqlUpdate()
    {
        $this->sql="UPDATE {$this->tbl} SET {$this->updates} WHERE ({$this->wc})";
        $sth = $this->dbObj->prepare($this->sql);
        $sth->execute();
        $this->sqlLastId = $sth->rowCount();
        $this->reSetSqlParams();
        unset($this->sql);
    }
    private function reSetSqlParams()
    {
        unset($this->tbl);
        unset($this->wc);
        unset($this->orderBy);
        unset($this->limit);
        unset($this->inserts);
        unset($this->updates);
        $this->distinctFieldsArray=[];
    }
    public function sqlSetBlindSql()
    {
        $sth = $this->dbObj->prepare($this->sql);
        $sth->execute();    
        unset($this->sql);
        return;   
    }
    public function sqlSetRecordset() //Done PDO
    {
        $this->buildGetSql();
        $this->recordSetArray=array();
        $sth = $this->dbObj->prepare($this->sql);
        $sth->execute();
        $this->recordCount=$sth->rowCount();
        $this->recordSetArray = $sth->fetchAll(PDO::FETCH_ASSOC);
        unset($this->sql);
        return;
    }
    public function traceObj(string $method, string $args="", string $caller="UnKnown") 
    {
        $sid=session_id();
        $mt=round(microtime(true) * 1000);//miliseconds
        $mt=substr($mt,-5);
        $context=getenv('context')."-$mt";
        $fullMethodArray=explode("::",$method);
        $method=$fullMethodArray[1];
        $obj=$fullMethodArray[0];
        $this->dbBase=$this->dbAppControl;
        $this->tbl='objs';
        $this->wc="(obj = '$obj') AND (method = '$method') AND (caller='$caller')";
        $this->limit="LIMIT 1";
        $this->connectDatabase();
        $this->sqlSetRecordset();
        if(count($this->recordSetArray)==1){      
            $cls=(int) $this->recordSetArray[0]['calls'];
            $cls++;
            $this->dbBase=$this->dbAppControl;
            $this->tbl='objs';            
            $this->wc="id = {$this->recordSetArray[0]['id']}";
            $this->updates="
                calls = $cls,
                args = '$args',
                session_id = '$sid',
                context='$context'
            ";
            $this->connectDatabase();
            $this->sqlUpdate();
            return;
        }
        $this->dbBase=$this->dbAppControl;
        $this->tbl='objs';            
        $this->inserts="
            obj = '$obj',
            method = '$method',
            caller = '$caller',
            args = '$args',
            calls = 1,
            session_id = '$sid',
            context='$context'
            ";
        $this->connectDatabase();
        $this->sqlInsert();    
        return;
    }      
}
