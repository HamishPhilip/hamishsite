<?php namespace prestationLogic;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
class Pl
{
    static public function registerPLVars(array $objVars, int $counter)     //2101      24
    {     
        $vars="";
        $c=1;
        foreach ($objVars as $key => $value) {
            $added=$value;
            if(is_array($value)){
                $added=implode(" # ",$value);
            } 
            $vars.="$key=$added|";
            $c++;
            if($c>$counter){
                break;
            }
        }
        return $vars;
    }
    static public function handleAlerts($caller){
        $msg="";
        if(isset($_GET['msg'])){
            $msg=$_GET['msg'];
            unset($_GET);
        }
        //alert from Session
        if(isset($_SESSION['alert'][$caller])){
            $msg=$_SESSION['alert'][$caller];
            unset($_SESSION['alert'][$caller]);
        }
        //alert from POST
        if(isset($_POST['alert'])){
            $msg=$_POST['alert'];
            unset($_POST['alert']);
        } 
        return $msg;
    }
    static public function evalPosts(array $posts){
        /*
            Type (If set)
            pst_type
            to|xx|yy    --  text Optional MinChars xx MaxChars yy   (xx or yy = 0 -> not set)
            tm|xx|yy    --  text Mandatory MinChars xx MaxChars yy
            no|xx|yy    --  numeric Optional MinVal xx MaxVal yy    (xx or yy = 0 ->not set)
            nm|xx|yy    --  numeric Mandated MinVal xx MaxVal yy    (xx or yy = 0 ->not set)
    [pst_from] => logInValidate.php
    [pst_token] => dcc509
    [pst_validate] => pst_token|tm|6|6


        */
        if(!isset($posts['pst_from'])){
            $_SESSION['alert']['index.php']="UnKnown capture or blank value submitted. Please repeat the last action.";
            header("location:index.php",301);
            exit();
        }
        $file=$_SERVER['DOCUMENT_ROOT']."/{$posts['pst_from']}";
        if(!file_exists($file)){
            $_SESSION['alert']['index.php']="UnKnown capture for ({$posts['pst_from']}).";
            header("location:index.php",301);
            exit();
        }
        if(isset($posts['pst_validate'])){
            $validateFields=explode("#",$posts['pst_validate']);
            for($v=0;$v<count($validateFields);$v++){
                $fieldParams=explode("|",$validateFields[$v]);
                $postField=$fieldParams[0];
                $type=$fieldParams[1];
                $min=$fieldParams[2];
                $max=$fieldParams[3];
            }
            switch ($type) {
                case "tm":  //text mandatory
                    if($min==$max){
                        $capureLen=strlen(trim($posts[$postField]));
                        if($capureLen != $min){
                            $_SESSION['alert'][$posts['pst_from']]="A captured value ($postField) is the incorrect length::".__LINE__;
                            header("location:{$posts['pst_from']}",301);
                            exit();
                        }
                    }
                    else
                    {
                        $_SESSION['alert']['index.php']="UnKnown hander check for tm here::".__LINE__;
                        header("location:index.php",301);
                        exit();
                    }
                    break;
                default:
                    $_SESSION['alert']['index.php']="UnKnown hander check for $type here::".__LINE__;
                    header("location:index.php",301);
                    exit();
            }
        }
        //email
        if(isset($posts['pst_email'])){
            $posAt=strpos($posts['pst_email'],"@");
            $posDot=strpos($posts['pst_email'],".");
            $result=$posAt*$posDot;
            if($result==0){
                $_SESSION['alert'][$posts['pst_from']]="eMail address Capture Appers Invalid.";
                header("location:{$posts['pst_from']}",301);
                exit();                
            }
        }
        return $posts;
    }
    static public function getEnv(string $context){
        $file=$_SERVER['DOCUMENT_ROOT']."/.env";
        if(!file_exists($file)){
            echo('FUBAR');
            exit();
        }
        $contents=file_get_contents($file);
        $arrayContents=explode("\n",$contents);
        foreach ($arrayContents as $key => $value) {
            $value=trim($value);
            $findEq=strpos($value,"=");
            if($findEq>0){
                $lineItemArray=explode("=",$value);
                $putEnvStr=trim($lineItemArray[0])."=".trim($lineItemArray[1]);
                $_SESIION['env'][]=$putEnvStr;
                putenv($putEnvStr);
            }
        }
        putenv("context=$context");
        putenv("EOF=true");
        return;
    }  
}